<?php

require_once('bootstrap.php');

// Routing 
if (isset($_GET['action'])) {
    $action = $_GET['action'];
} else {
    $action = 'index';
}


// Get controller
$config = parse_ini_file('config.ini');
if ($action == 'error')
{
    $controller = new ErrorController($config);
    $action = 'index';
}
else
{
    $controller = new HomeController($config);
}

// Load action
$controller->$action();