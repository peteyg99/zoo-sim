<?php

class Elephant extends BaseAnimal
{
    /**
     * Create a new BaseAnimal instance.
     *
     * @param int $increment
     * @return void
     */
    function __construct($increment) 
    {
        parent::__construct($increment);

        // Set animal specific data
        $this->type = "Elephant";
        $this->minHealth = 70;
    }

    /**
     * Override of method in BaseAnimal
     * Check if this animal is dead.
     *
     * @param float $modifyValue
     * @return bool
     */
    protected function checkIfDead($modifyValue)
    {
        // Get previous health value
        $prevValue = $this->healthValue - $modifyValue;

        // Elephant is dead if it has been below minValue for over an hour
        if ($prevValue < $this->minHealth
            && $this->healthValue < $this->minHealth)
        {
            $this->isDead = true;
        }
    }
}