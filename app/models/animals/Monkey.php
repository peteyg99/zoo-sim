<?php

class Monkey extends BaseAnimal
{
    /**
     * Create a new BaseAnimal instance.
     *
     * @param int $increment
     * @return void
     */
    function __construct($increment) 
    {
        parent::__construct($increment);

        // Set animal specific data
        $this->type = "Monkey";
        $this->minHealth = 30;
    }
}