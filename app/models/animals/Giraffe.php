<?php

class Giraffe extends BaseAnimal
{
    /**
     * Create a new BaseAnimal instance.
     *
     * @param int $increment
     * @return void
     */
    function __construct($increment) 
    {
        parent::__construct($increment);

        // Set animal specific data
        $this->type = "Giraffe";
        $this->minHealth = 50;
    }
}