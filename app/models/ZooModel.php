<?php 

class ZooModel 
{
    public $animals = array();

    private $animalCategories = array(
        'Monkey',
        'Giraffe',
        'Elephant'
    );

    private $numEachAnimal = 5;
    private $defaultTimeIncrement = "1 hour";

    private $startTime;
    private $currentTime;


    /**
     * Create a new ZooModel instance.
     *
     * @return void
     */
    function __construct($config) 
    {
        // Get config
        $this->setZooConfig($config);

        // Setup animals
        foreach ($this->animalCategories as $animalCategory)
        {
            for ($i = 0; $i < $this->numEachAnimal; $i++)
            {
                $this->animals[$animalCategory][] = new $animalCategory($i);
            }
        }

        $this->startClock();
    }


    /**
     * Set zoo config items
     *
     * @param array $config
     * @return void
     */
    private function setZooConfig($config)
    {
        if (!$config || !is_array($config)) return;
        foreach ($config as $configKey => $configItem)
        {
            if (isset($this->$configKey))
            {
                $this->$configKey = $configItem;
            }
        }
    }


    /**
     * Start the clock.
     *
     * @return void
     */
    public function startClock()
    {
        $this->startTime = new DateTime();
        $this->currentTime = $this->startTime;
    }


    /**
     * Increase time by default amount, and reduce animal health.
     *
     * @return void
     */
    public function incrementTime()
    {
        $this->currentTime = $this->currentTime->modify($this->defaultTimeIncrement);

        foreach ($this->animals as $animalCategory)
        {
            foreach ($animalCategory as $animal)
            {
                $randomValue = $this->getRandomNumber(0, 20);
                $animal->modifyHealth(-$randomValue);
            }
        }
    }

    /**
     * Feed animals, by category.
     *
     * @return void
     */
    public function feedAnimals()
    {
        foreach ($this->animals as $animalCategory)
        {
            $randomValue = $this->getRandomNumber(10, 25);
            foreach ($animalCategory as $animal)
            {
                $animal->modifyHealth($randomValue);
            }
        }
    }    

    private function getRandomNumber($min, $max)
    {
        return $min + (
            ((float) mt_rand() / (float) mt_getrandmax()) 
                * ($max - $min)
            );
    }

    /**
     * Return formatted time.
     *
     * @return string
     */
    public function getTime()
    {
        if (!$this->currentTime) print "no time!"; // TODO: handle error
        return $this->currentTime->format("D jS M Y - H:i");
    }
}



