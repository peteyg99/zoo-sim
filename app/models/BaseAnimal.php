<?php

abstract class BaseAnimal
{
    protected $healthValue = 100;
    protected $number;
    protected $type = 'not set';
    protected $isDead = false;
    protected $minHealth = 0;

    /**
     * Create a new BaseAnimal instance.
     *
     * @param int $increment
     * @return void
     */
    function __construct($increment) 
    {
        // Set animal's number
        $this->number = $increment + 1;

        // Enforce float type
        settype($this->healthValue, "float");
    }

    /**
     * Return the name of this animal.
     *
     * @return string
     */
    public function getName()
    {
        return $this->type . " " . $this->number;
    }

    /**
     * Return the health status of this animal.
     *
     * @return string
     */
    public function getHealth()
    {
        if ($this->isDead) return "Dead!";
        return $this->healthValue . "%";
    }

    /**
     * Return the health value of this animal.
     *
     * @return float
     */
    public function getHealthValue()
    {
        return $this->healthValue;
    }
    
    /**
     * Modify the health for this animal.
     *
     * @param float $modifyValue
     * @return void
     */
    public function modifyHealth($modifyValue)
    {
        // If animal is dead, it can't resurrected!
        if ($this->isDead) return;

        // Adjust health
        $this->healthValue += $modifyValue;

        // Max health 100%
        if ($this->healthValue > 100) $this->healthValue = 100;

        // Is it dead?
        $this->checkIfDead($modifyValue);
    }

    /**
     * Check if this animal is dead.
     * ($modifiedValue is passed to be used in overrides)
     *
     * @param float $modifyValue
     * @return bool
     */
    protected function checkIfDead($modifyValue)
    {
        if ($this->healthValue < $this->minHealth)
        {
            $this->isDead = true;
        }
        return $this->isDead;
    }


    /**
     * Tests if this animal is dead.
     *
     * @return bool
     */
    public function getIfDead()
    {
        return $this->isDead;
    }
}
