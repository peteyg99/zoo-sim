<?php

// Preload files
$foldersToPreload = array(
    'controllers',
    'models',
    'models/animals'
);

foreach ($foldersToPreload as $folderName)
{
    $directoryPath = __DIR__ . '/' . $folderName;
    foreach (scandir($directoryPath) as $filename) 
    {
        $path = $directoryPath . '/' . $filename;
        if (is_file($path)) {
            require_once $path;
        }
    }
}

