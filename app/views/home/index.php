<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>ZooSim</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    </head>

    <body>

        <nav class="navbar navbar-default">
            <div class="container">
                <a class="navbar-brand" href="<?php print $pageVars->linkPaths->home; ?>">ZooSim</a>
            </div>
        </nav>
        
        <div class="container">


            <div class="row">
                <aside class="col-sm-4">
                
                    <div class="well">
                        <strong>Time:</strong> <?php print $pageVars->zooTime; ?>
                    </div>             

                    <p>
                        <a class="btn btn-primary" href="<?php print $pageVars->linkPaths->next; ?>">Next hour</a>
                        <a class="btn btn-primary" href="<?php print $pageVars->linkPaths->feed; ?>">Feed zoo</a>
                        <a class="btn btn-danger" href="<?php print $pageVars->linkPaths->reset; ?>">Reset</a>
                    </p>

                </aside>             

                <main class="col-sm-4">

                    <!-- Zoo animals status table -->

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Animal</th>
                                <th>Health</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($pageVars->animals as $animalCategory): ?>
                                <?php foreach ($animalCategory as $animal): ?>

                                    <tr>
                                        <td><?php print $animal->getName(); ?></td>
                                        <td><?php print $animal->getHealth(); ?></td>
                                    </tr>

                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                </main>

            </div>
        </div>
    </body>
</html>