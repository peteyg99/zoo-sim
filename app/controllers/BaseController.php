<?php 

class BaseController 
{
    protected $config;
    private $baseUrl;
    private $basePath = '';
    private $magicRouting = false;


    /**
     * Create a new BaseController instance.
     *
     * @param array $config
     * @return void
     */
    function __construct($config) 
    {
        $this->setConfig($config);

        // Set site's base URL
        $this->baseUrl = 'http://' . $_SERVER['HTTP_HOST'] . $this->basePath;
    }


    /**
     * Set routing config.
     *
     * @param array $config
     * @return void
     */
    private function setConfig($config)
    {
        // Store config
        $this->config = $config;
        if (!$config || !is_array($config)) return;

        // Set routing
        if (isset($config['basePath'])) $this->basePath = $config['basePath'];
        if (isset($config['magicRouting'])) $this->magicRouting = $config['magicRouting'];
    }


    /**
     * Super basic templating.
     *
     * @param string $templatePath
     * @param object $pageVars
     * @return void
     */
    protected function view($templatePath, $pageVars)
    {
        // Load template
        require_once('../app/views/' . $templatePath . '.php');
    }


    /**
     * Get formatted path.
     *
     * @param string $path
     * @return void
     */
    protected function getLinkPath($path = null) 
    {
        // Magic routing
        if ($this->magicRouting || !$path) return $this->baseUrl . $path;

        // Standard routing, if no mod_rewrite
        return $this->baseUrl . '?action=' . $path;
    }


    /**
     * Do relative HTTP redirect.
     *
     * @param string $redirectPath
     * @return void
     */
    protected function doRedirect($redirectPath = '') 
    {
        $redirectPath = $this->getLinkPath($redirectPath);
        header('Location: ' . $redirectPath);
    }

}