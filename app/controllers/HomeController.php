<?php 

class HomeController extends BaseController
{
    private $zoo;


    /**
     * Create a new HomeController instance.
     *
     * @param array $config
     * @return void
     */
    function __construct($config) 
    {
        try 
        {
            parent::__construct($config);
    
            // Start session
            session_start();

            // Get/create zoo
            $this->zoo = $this->getZooModel();
        }
        catch (Exception $ex)
        {
            $this->processError();
        }
    }


    /**
     * Default action, show zoo stats.
     *
     * @return void
     */
    public function index() 
    {
        try 
        {
            // Get together variables to pass to view
            $pageVars = (object) array(
                'linkPaths' => (object) array(
                    'home' => $this->getLinkPath(),
                    'next' => $this->getLinkPath('next'),
                    'feed' => $this->getLinkPath('feed'),
                    'reset' => $this->getLinkPath('reset')
                ),
                'zooTime' => $this->zoo->getTime(),
                'animals' => $this->zoo->animals
            );

            // Show view
            $this->view('home/index', $pageVars);
        }
        catch (Exception $ex)
        {
            $this->processError();
        }
    }


    /**
     * Increment time in zoo.
     *
     * @return void
     */
    public function next() 
    {
        try 
        {
            $this->zoo->incrementTime();
            $this->doRedirect();
        }
        catch (Exception $ex)
        {
            $this->processError();
        }
    }


    /**
     * Feed zoo animals.
     *
     * @return void
     */
    public function feed() 
    {
        try 
        {
            $this->zoo->feedAnimals();
            $this->doRedirect();
        }
        catch (Exception $ex)
        {
            $this->processError();
        }
    }


    /**
     * Reset zoo & stats.
     *
     * @return void
     */
    public function reset() 
    {
        try 
        {
            $this->createZoo();
            $this->doRedirect();
        }
        catch (Exception $ex)
        {
            $this->processError();
        }
    }


    /**
     * Get ZooModel, create
     * if none exists in PHP session.
     *
     * @return ZooModel
     */
    private function getZooModel() 
    {
        // Attempt to get from session
        $zoo = $this->getZooFromSession();

        if (!$zoo || !($zoo instanceof ZooModel))
        {            
            $zoo = $this->createZoo();
        }

        return $zoo;
    }


    /**
     * Get ZooModel from PHP session.
     *
     * @return ZooModel
     */
    private function getZooFromSession() 
    {
        if (isset($_SESSION["zoo"]))
        {
            return $_SESSION["zoo"];
        }
        return null;
    }


    /**
     * Get create new ZooModel and add to PHP session.
     *
     * @return ZooModel
     */
    private function createZoo() 
    {
        $zoo = new ZooModel($this->config);
        $_SESSION["zoo"] = $zoo;
        return $zoo;
    }


    /**
     * Room to implement extra error handling here,
     * currently just redirecting.
     * 
     * @return void
     */
    private function processError()
    {
        $this->doRedirect('error');
    }
}
