<?php

date_default_timezone_set('UTC');
require_once('../bootstrap.php');

class ZooTests extends PHPUnit_Framework_TestCase
{

    private function getNewZoo()
    {
        $config = parse_ini_file('../config.ini');
        $zoo = new ZooModel($config);
        return $zoo;
    }

    public function testZooIsCreated()
    {
        $zoo = $this->getNewZoo();
        $this->assertTrue($zoo instanceof ZooModel);
        return $zoo;
    }

    /**
     * @depends testZooIsCreated
     */
    public function testThereAreFiveTypesOfEachAnimal()
    {
        $zoo = func_get_args()[0];

        $animalCounts = array();
        foreach ($zoo->animals as $type => $animalCategory)
        {
            foreach ($animalCategory as $animal)
            {
                $animalCounts[$type]++;
            }
        }
        foreach ($animalCounts as $animalCount)
        {
            $this->assertEquals($animalCount, 5, "Incorrect number of animals");
        }
    }

    /**
     * @depends testZooIsCreated
     */
    public function testEveryAnimalStartsAtFullHealth()
    {
        $zoo = func_get_args()[0];

        foreach ($zoo->animals as $type => $animalCategory)
        {
            foreach ($animalCategory as $animal)
            {
                $this->assertSame($animal->getHealth(), '100%', "Incorrect health starting status");
                $this->assertEquals($animal->getHealthValue(), 100, "Incorrect health starting value");
            }
        }
    }    

    /**
     * @depends testZooIsCreated
     */
    public function testIncrementingTimeReducesHealth()
    {
        $zoo = func_get_args()[0];
        $zoo->incrementTime();

        foreach ($zoo->animals as $type => $animalCategory)
        {
            foreach ($animalCategory as $animal)
            {
                $this->assertLessThan(100, $animal->getHealthValue(), "Incrementing time has not reduced health");
                $this->assertGreaterThan(80, $animal->getHealthValue(), "Incrementing time has reduced health by too much");
            }
        }

        return $zoo;
    }    

    /**
     * @depends testIncrementingTimeReducesHealth
     */
    public function testFeedingIncreasesHealth()
    {
        $zoo = func_get_args()[0];
        $zoo->incrementTime();

        $animalHealth = array();
        foreach ($zoo->animals as $type => $animalCategory)
        {
            foreach ($animalCategory as $animal)
            {
                $animalHealth[$animal->getName()] = $animal->getHealthValue();
            }
        }

        $zoo->feedAnimals();

        foreach ($zoo->animals as $type => $animalCategory)
        {
            foreach ($animalCategory as $animal)
            {
                $prevHealth = $animalHealth[$animal->getName()];
                $newHealth = $animal->getHealthValue();
                $this->assertGreaterThan($prevHealth, $newHealth, "Feeding did not increase health");

                $healthIncrease = $newHealth - $prevHealth;
                $this->assertLessThan(25, $healthIncrease, "Feeding increased health by too much");
                if ($newHealth < 100)
                {
                    $this->assertGreaterThan(10, $healthIncrease, "Feeding increased health by too little");
                }
            }
        }
    }        


    public function testMonkeysDieCorrectly()
    {
        $zoo = $this->getNewZoo();
        $monkey = $this->getAnimalAndReduceHealth($zoo, Monkey, 30);
        $this->assertTrue($monkey->getIfDead(), "Should be dead");
    }

    /**
     * @depends testZooIsCreated
     */
    public function testGiraffesDieCorrectly()
    {
        $zoo = $this->getNewZoo();
        $giraffe = $this->getAnimalAndReduceHealth($zoo, Giraffe, 50);
        $this->assertTrue($giraffe->getIfDead(), "Should be dead");
    }
    
    /**
     * @depends testEveryAnimalStartsAtFullHealth
     */
    public function testElephantsDieCorrectly()
    {
        $zoo = $this->getNewZoo();
        $elephant = $this->getAnimalAndReduceHealth($zoo, Elephant, 70);
        $this->assertFalse($elephant->getIfDead(), "Shouldn't be dead");
        $zoo->incrementTime();
        $this->assertTrue($elephant->getIfDead(), "Should be dead");
    }


    private function getAnimalAndReduceHealth($zoo, $animalType, $healthValue)
    {
        $getAnimal = null;

        foreach ($zoo->animals as $type => $animalCategory)
        {
            foreach ($animalCategory as $animal)
            {
                if ($animal instanceof $animalType)
                {
                    $getAnimal = $animal;
                    break 2;
                }
            }
        }

        $this->assertNotNull($getAnimal, "Animal not found in zoo");
        $this->assertTrue($getAnimal instanceof $animalType, "Animal is wrong type?!");

        while (!$getAnimal->getIfDead() && $getAnimal->getHealthValue() > $healthValue)
        {
            $zoo->incrementTime();
        }
        $this->assertLessThan($healthValue, $getAnimal->getHealthValue(), "Error decreasing animal health");

        return $getAnimal;
    }
    
}